import * as fromCitiesList from './cities-weather-list.reducer';
import { ActionReducerMap, createFeatureSelector, createSelector } from '@ngrx/store';

export interface State {
    cities: fromCitiesList.CitiesWeatherListState;
}

export const reducers: ActionReducerMap<State> = {
    cities: fromCitiesList.citiesWeatherDataListreducer,
};

export const selectCitiesState = createFeatureSelector<State>('weatherData');

export const getCitiesState = createSelector(
    selectCitiesState,
    state => state ? state.cities.cities : [],
);
