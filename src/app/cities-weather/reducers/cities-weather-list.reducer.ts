import { CitiesWeatherListActionsUnion, CitiesWeatherListTypes, AddOrUpdateCity } from '../actions/cities-weather-list.actions';
import { CityWeatherData } from '../models/CityWeatherData';
export interface CitiesWeatherListState {
    cities: CityWeatherData[];
}

const initialState: CitiesWeatherListState = {
    cities: [],
};

export function citiesWeatherDataListreducer(state: CitiesWeatherListState = initialState,
    action: CitiesWeatherListActionsUnion): CitiesWeatherListState {
    console.log(JSON.stringify(action.payload));
    switch (action.type) {
        case CitiesWeatherListTypes.AddOrUpdateCity: {
            const itemIndexInArray = state.cities.findIndex((item) => { console.log(item.id); return item.id === action.payload.id });
            console.log("index:" + itemIndexInArray);
            if (itemIndexInArray === -1) { return { ...state, cities: [...state.cities, action.payload] }; } else {
                return {
                    ...state, cities: state.cities.map((city) => {
                        if (city.id === action.payload.id) {
                            return action.payload;
                        } else { return city; }
                    })
                };
            }

        }
        case CitiesWeatherListTypes.ClearAll: {
            return initialState;
        }
        case CitiesWeatherListTypes.DeleteCity: {
            return {
                ...state, cities: state.cities.filter((city) => {
                    if (!(city.id === action.payload.id)) {
                        return city;
                    }
                }
                })
        };
    }
        default:
    return state;
}

}


