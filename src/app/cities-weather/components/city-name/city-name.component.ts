import { Component, OnInit } from '@angular/core';
import { CityWeatherData } from '../../models/CityWeatherData';
import { Input } from '@angular/core';
@Component({
  selector: 'app-city-name',
  templateUrl: './city-name.component.html',
  styleUrls: ['./city-name.component.css']
})
export class CityNameComponent implements OnInit {
  @Input() cityWeatherData: CityWeatherData;
  constructor() {
  }

  getName() {
    return this.cityWeatherData.name;
  }

  ngOnInit() {
  }

}
