import { Component, OnInit, Input } from '@angular/core';
import { CityWeatherData } from '../../models/CityWeatherData';

@Component({
  selector: 'app-city-weather-data',
  templateUrl: './city-weather-data.component.html',
  styleUrls: ['./city-weather-data.component.css']
})
export class CityWeatherDataComponent implements OnInit {
  @Input() cityWeatherData: CityWeatherData;
  constructor() {
  }

  ngOnInit() {
  }
  getCalculationDate() {
    return new Date(this.cityWeatherData.dt);
  }
}
