import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CityWeatherDataComponent } from './city-weather-data.component';

describe('CityWeatherDataComponent', () => {
  let component: CityWeatherDataComponent;
  let fixture: ComponentFixture<CityWeatherDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CityWeatherDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CityWeatherDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
