import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CitiesWeatherListComponent } from './containers/cities-weather-list/cities-weather-list.component';
import { CityNameComponent } from './components/city-name/city-name.component';
import { CityWeatherDataComponent } from './components/city-weather-data/city-weather-data.component';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { DataListModule } from 'primeng/datalist';
import { ProgressBarModule } from 'primeng/progressbar';
import { StoreModule } from '@ngrx/store';
import { reducers } from './reducers';
@NgModule({
  imports: [
    CommonModule,
    DataListModule,
    StoreModule.forFeature('weatherData', reducers),
    ButtonModule, InputTextModule, ProgressBarModule
  ],
  declarations: [CitiesWeatherListComponent, CityNameComponent, CityWeatherDataComponent],
  exports: [CitiesWeatherListComponent, CityNameComponent, CityWeatherDataComponent]
})
export class CitiesWeatherModule { }
