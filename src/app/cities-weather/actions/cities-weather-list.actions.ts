import { Action } from '@ngrx/store';
import { CityWeatherData } from '../models/CityWeatherData';

export enum CitiesWeatherListTypes {
    AddOrUpdateCity = '[CitiesWeatherList] Add city',
    DeleteCity = '[CitiesWeatherList] Delete city',
    ClearAll = '[CitiesWeatherList] Clear all'
}


export class AddOrUpdateCity implements Action {
    readonly type = CitiesWeatherListTypes.AddOrUpdateCity;
    constructor(public payload: CityWeatherData) { }
}

export class DeleteCity implements Action {
    readonly type = CitiesWeatherListTypes.DeleteCity;
    constructor(public payload: CityWeatherData) { }
}

export class ClearAll implements Action {
    readonly type = CitiesWeatherListTypes.ClearAll;
    constructor(public payload?: any) { }
}

export type CitiesWeatherListActionsUnion = | AddOrUpdateCity | DeleteCity | ClearAll;
