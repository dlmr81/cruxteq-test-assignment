import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { CityWeatherData } from '../../models/CityWeatherData';
import { Store, select } from '@ngrx/store';
import * as fromCitiesListActions from '../../actions/cities-weather-list.actions';
import * as fromCitiesListReducer from '../../reducers/cities-weather-list.reducer';
import { getCitiesState } from '../../reducers/index';
import getWeatherData from '../../api.weather';
import { DataList } from 'primeng/datalist';
import { CityWeatherDataComponent } from '../../components/city-weather-data/city-weather-data.component';
import { Subscription } from 'rxjs/Subscription';
@Component({
  selector: 'app-cities-weather-list',
  templateUrl: './cities-weather-list.component.html',
  styleUrls: ['./cities-weather-list.component.css']
})
export class CitiesWeatherListComponent implements OnInit {
  public citiesWeather$: Observable<CityWeatherData[]>;
  constructor(private store: Store<fromCitiesListReducer.CitiesWeatherListState>) {
    this.citiesWeather$ = store.pipe(select(getCitiesState));

  }

  ngOnInit() {
  }


  addOrUpdateCity(cityName: string) {
    getWeatherData(cityName).then((res) => {
      console.log(this.citiesWeather$);
      const apiSuccessfullCallCode = '200';
      if (res.cod === apiSuccessfullCallCode) {
        this.store.dispatch(new fromCitiesListActions.AddOrUpdateCity(res));
      } else {
        console.log(res);
      }
    }, (err) => { });
  }
  deleteCity(cityToDelete: CityWeatherData) {
    this.store.dispatch(new fromCitiesListActions.DeleteCity(cityToDelete));
  }

  clearAll() {
    this.store.dispatch(new fromCitiesListActions.ClearAll());
  }

  handleSearchClick(event) {
    event.preventDefault();
    const cityNameValue: string = (document.getElementById('cityInput') as HTMLInputElement).value;
    this.addOrUpdateCity(cityNameValue);
  }

  handleDeleteClick(event, data?: CityWeatherData) {
    event.preventDefault();
    this.deleteCity(data);
  }

  handleUpdateClick(event, data?: CityWeatherData) {
    event.preventDefault();
    this.addOrUpdateCity(data.name);
  }

}

