import * as weatherAPI from 'npm-openweathermap';
const API_KEY = '68793f1e7e8d0cde8fa39c0be67dd40a';
/*default value*/
weatherAPI.api_key = API_KEY;
weatherAPI.units = "metric";

function getWeatherData(cityName: string): any {
    return weatherAPI.get_weather_custom('city', cityName, 'weather');
}
export default getWeatherData;

